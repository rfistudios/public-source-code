var Home = {

	slider: '',
	
	init: function() {
		
		slider = $('#our-story').html();

		Home.resizeContainer();
		$(window).bind('resize', function(){ Home.resizeContainer(); });
	},


	resizeContainer: function() {
		
		if( $('#break2').is(':visible') ) {
        	
        	if( $('div.slick-slide').length == 0 ) {
	        	
	        	$('.slider-4-cols').slick({
	    			arrows: false,
	    			dots: true
	    		});
	        }
        }
        else
        {
        	if( $('div.slick-slide').length > 0 )
        	{
        		$('#our-story').html(slider);
        	}
        }
		

	}


}

Home.init();


/*
  		});*/