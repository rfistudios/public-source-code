/* ----------------------------------------
  Global
------------------------------------------ */
var Global = {

  player: null,
  APIModules: null,
  videoPlayers: [],
  experienceModules: [],

  init: function() {
    //console.log('global init');
    $(window).bind('resize', function() {
      Global.resizeContainer();
    });
    Global.resizeContainer();
  },

  onTemplateLoad: function(experienceID) {

    Global.player = brightcove.api.getExperience(experienceID);
    Global.APIModules = brightcove.api.modules.APIModules;
    Global.videoPlayers.push(Global.player.getModule(Global.APIModules.VIDEO_PLAYER));
    Global.experienceModules.push(Global.player.getModule(Global.APIModules.EXPERIENCE));

  },

  onTemplateReady: function(evt) {

  },

  resizeContainer: function() {

    for (var i = 0; i < Global.experienceModules.length; i++) {

      resizeWidth = $(".BrightcoveExperience").eq(i).width(),
        resizeHeight = $(".BrightcoveExperience").eq(i).height();

      if (Global.experienceModules[i].experience.type == "html") {
        Global.experienceModules[i].setSize(resizeWidth, resizeHeight);
      }
    }

    if (Global.isMobile()) {

      // specify order of primary and secondary nav based on layout
      $('#secondary').insertBefore('#primary');
      $('#search').insertBefore('#secondary');
      $('.global').insertBefore('.more-from-site');

    } else {

      // specify order of primary and secondary nav based on layout
      $('#primary').insertBefore('#secondary');
      $('#search').prependTo('#primary');
      $('.global').insertAfter('.more-from-site');

      // clean up any classes or elements we added in mobile nav
      $('.offscreen-left').removeClass('offscreen-left');
      $('header').removeClass('expanded');
      $('.menu-button').text('Menu');
      $('header .active').removeClass('active');
      $('.mobile-nav-tertiary-header').remove();
      $('.mobile-nav-back-button').remove();

      // select first item in meganav
      $('.tertiary .links li:first-child').addClass('active');
    }

    Global.arrangeDropdownColumns();
  },

  isTablet: function() {
    return $('#break1').is(':visible');
  },

  isMobile: function() {
    return $('#break2').is(':visible');
  },

  // arrange language and "more from site" dropdown list items into appropriate number of columns
  arrangeDropdownColumns: function() {

    var numMobileColumns = 1;
    var numTabletColumns = 2;
    var numDesktopColumns = 3;

    $('.primary-nav-dropdown').each(function() {

      var $listItems = $('.right-column li', $(this));
      var numColumns;

      if (Global.isMobile()) {
        numColumns = numMobileColumns;
      } else if (Global.isTablet()) {
        numColumns = numTabletColumns;
      } else {
        numColumns = numDesktopColumns;
      }

      // if we already have the correct number of columns, return
      if ($('.right-column ul', $(this)).length === numColumns) {
        return;
      }

      // set up empty 2d columns array
      var columns = [];
      for (var i = 0; i < numColumns; i++) {
        columns.push([]);
      }

      // divide list items into arrays for each column
      for (var i = 0; i < $listItems.length; i++) {
        columns[Math.floor(i * columns.length / $listItems.length)].push($($listItems[i]).html());
      }

      // make lists for each column
      var lists = '';
      for (var i = 0; i < numColumns; i++) {
        lists += '<ul><li>' + columns[i].join('</li><li>') + '</li></ul>';
      }

      // stick the lists into the dom
      $('.right-column', $(this)).html(lists);
    });
  }
}

Global.init();

$(document).ready(function() {

  // handle secondary nav click
  $('#main header nav#secondary ul li.grouping a').on('click', function(event) {

    var grouping = $(this).data('grouping');

    // handle mobile click
    if (Global.isMobile()) {

      $('header .tertiary[data-grouping="' + grouping + '"]').addClass('active');
      $('#primary, #secondary').addClass('offscreen-left');

      var sectionName = grouping;
      $('header .tertiary[data-grouping="' + grouping + '"] .links ul').prepend('<li class="mobile-nav-tertiary-header">' + sectionName + '</li>');
      $('header .tertiary[data-grouping="' + grouping + '"] .links ul').prepend('<li class="mobile-nav-back-button"><a href="#">Back to Main Menu</a></li>');

      // handle desktop/tablet click
    } else {

      $('#main header nav#secondary ul li.grouping').not('a[data-grouping="' + grouping + '"]').removeClass('active');

      $('#main header .tertiary').not('[data-grouping="' + grouping + '"]').removeClass('active').fadeOut('slow', function() {
        $('#main header nav#secondary ul li.grouping a[data-grouping="' + grouping + '"]').parent().addClass('active');

        if ($('#main header .tertiary[data-grouping="' + grouping + '"] .content nav.links ul li:first-child').hasClass('active')) {
          $('#main header .tertiary[data-grouping="' + grouping + '"] .content nav.quaternary ul:first-child').show();
        }

        $('#main header .tertiary[data-grouping="' + grouping + '"]').addClass('active').slideDown('slow', function() {
          $(this).on('mouseleave', function(event) {
            var grouping = $('#main header nav#secondary ul li.grouping a').data('grouping');

            $('#main header nav#secondary ul li.grouping').removeClass('active');

            $('#main header .tertiary').removeClass('active').fadeOut('slow');
          });
        });
      });
    }

    event.preventDefault();
  });

  // handle mobile nav back button click
  $('header ul').on('click', '.mobile-nav-back-button', function() {

    // if clicked from quaternary nav
    if ($(this).parents('.quaternary').length > 0) {
      $('.quaternary ul').removeClass('active');
      $('.tertiary').removeClass('offscreen-left');

      // else clicked from tertiary nav
    } else {
      $('.tertiary').removeClass('active');
      $('#primary, #secondary').removeClass('offscreen-left');
    }

    $(this).parents('ul').find('.mobile-nav-tertiary-header').remove();
    $(this).parents('ul').find('.mobile-nav-back-button').remove();

    event.stopPropagation();
    return false;
  });

  // handle tertiary nav click
  $('#main header .tertiary .content nav.links ul li a').on('click mouseover', function(event) {

    var parent_grouping = $(this).parents('.tertiary').data('grouping');
    var grouping = $(this).data('grouping');

    // handle mobile click
    if (Global.isMobile()) {

      // ignore mouseover because we'll get weird behavior on a desktop with a mobile sized window
      if (event.type === 'click') {
        $(this).parents('.tertiary').addClass('offscreen-left');
        $(this).parents('.links').siblings('.quaternary').find('ul[data-grouping="' + grouping + '"]').addClass('active');

        var sectionName = grouping;
        $(this).parents('.links').siblings('.quaternary').find('ul[data-grouping="' + grouping + '"]').prepend('<li class="mobile-nav-tertiary-header">' + sectionName + '</li>');
        $(this).parents('.links').siblings('.quaternary').find('ul[data-grouping="' + grouping + '"]').prepend('<li class="mobile-nav-back-button"><a href="#">Back</a></li>');
      }

      // handle desktop/tablet click
    } else {

      $('#main header .tertiary[data-grouping="' + parent_grouping + '"] .content nav.links ul li a').not('[data-grouping="' + grouping + '"]').parent().removeClass('active');
      $('#main header .tertiary[data-grouping="' + parent_grouping + '"] .content nav.quaternary ul').not('[data-grouping="' + grouping + '"]').hide();
      $('#main header .tertiary[data-grouping="' + parent_grouping + '"] .content nav.links ul li a[data-grouping="' + grouping + '"]').parent().addClass('active');
      $('#main header .tertiary[data-grouping="' + parent_grouping + '"] .content nav.quaternary ul[data-grouping="' + grouping + '"]').fadeIn('slow');

      event.preventDefault();
    }
  });

  // cancel clicks in the primary nav dropdown so they dont make the dropdown close
  $('.primary-nav-dropdown').on('click', function(event) {
    event.stopPropagation();
  });

  // handle top nav dropdowns
  $('.primary-nav-dropdown-target').on('click', function(event) {

    // handle mobile layout
    if (Global.isMobile()) {

      $(this).addClass('active');
      $('#primary .utilities, #secondary').addClass('offscreen-left');
      $('.primary-nav-dropdown-target').not($(this)).addClass('offscreen-left');

      // add back button and section name
      var sectionName = $(this).find('a').first().text();
      $('ul', $(this)).prepend('<li class="mobile-nav-tertiary-header">' + sectionName + '</li>');
      $('ul', $(this)).prepend('<li class="mobile-nav-back-button"><a href="#">Back to Main Menu</a></li>');

      $('.primary-nav-dropdown .mobile-nav-back-button').on('click', function() {

        $(this).parents('.primary-nav-dropdown-target').removeClass('active');
        $('#primary .utilities, #secondary').removeClass('offscreen-left');
        $('.primary-nav-dropdown-target').removeClass('offscreen-left');

        // remove extra buttons and click listener
        $(this).parents('ul').find('.mobile-nav-tertiary-header').remove();
        $(this).parents('ul').find('.mobile-nav-back-button').remove();
        $(this).off('click');
      });

      // handle tablet/desktop layout
    } else {

      var isOpen = $(this).hasClass('active');

      // close all open dropdowns
      $('.primary-nav-dropdown-target.active').each(function() {
        closeDropdown($(this));
      });

      // if this dropdown was closed, open it
      if (!isOpen) {

        var $self = $(this);
        $self.addClass('active');
        $('.primary-nav-dropdown', $self).slideDown('slow', function() {

          // close dropdown when mouse leaves
          $(this).on('mouseleave', function(event) {
            closeDropdown($self);
          });
        });
      }
    }

    return false;
  });

  function closeDropdown(dropdown) {
    dropdown.removeClass('active');
    $('.primary-nav-dropdown', dropdown).fadeOut('slow');
    $(this).off('mouseleave');
  }

  // mobile nav
  $('.menu-button').on('click', function() {
    $('header').toggleClass('expanded');

    if ($('header').hasClass('expanded')) {
      $('.menu-button').text('Close');
    } else {
      $('.menu-button').text('Menu');
    }
  });

  // footer nav
  $('footer section h4').on('click', function() {

    if (Global.isMobile()) {

      $('footer section h4').not('.footer-contact h4').hide();
      $(this).siblings('ul').show();

      var sectionName = $(this).text();
      $(this).siblings('ul').prepend('<li class="mobile-nav-tertiary-header">' + sectionName + '</li>');
      $(this).siblings('ul').prepend('<li class="mobile-nav-back-button"><a href="#">Back</a></li>');

      return false;
    }
  });

  $('footer ul').on('click', '.mobile-nav-back-button', function() {

    if (Global.isMobile()) {

      $('footer section h4').show();
      $('footer section ul').hide();

      $(this).parents('ul').find('.mobile-nav-tertiary-header').remove();
      $(this).parents('ul').find('.mobile-nav-back-button').remove();

      return false;
    }
  });
});